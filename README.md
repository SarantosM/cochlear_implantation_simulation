# Cochlear_Implantation_Simulation
Welcome to my Master Thesis repository of a 3D time-dependent Finite Element Analysis of Cochlear Implantation. 
The numerical algorithms for developing the Information Encoding of any audio sample into a form applicable for
a FEM current conduction are provided. 
Master Thesis is available upon request. If you require further information contact me at sarantos.mantzagriotis01@estudiant.upf.edu

## 3D geometry

![image alt ><](/Figures/CI_Head.png)

![image alt ><](/Figures/Cochlea.png)

## Information Encoding - From .mp3 to Electrodogram
**N-of-M Spectral Strategy**

The Averaged per Electrode Bandwidth Short-Time-Fourier Transform Identifies that in all buffers the power is dominated by the first four electrodes.
Electrode 1 to 4 encode frequencies from 70 to 690 Hz.

![image alt ><](/Figures/Spec2.PNG)

In Time-domain the gammatone filter banks

![image alt ><](/Figures/gtone.png)

The Hilbert Transform allows to extract the envelope detection and Fine Structure. This is applied into the narrowband filter banks for Electrodes 1 to 4.

![image alt ><](/Figures/hilbert.png)

## Finite Element Transient Current Conduction

![Alt Text ><](CI_Space.gif)

## Neural Model

![image alt ><](/Figures/ANF.PNG)

The time dependent electrical fields are passed into a Modified Hodgkin-Huxley compartment model.

