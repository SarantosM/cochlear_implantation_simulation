
import numpy as np
#import sys

#cd(os.getcwd()) #  |Gets current directory

#filename = sys.argv[0] 
filename = "Cmaj7sin.txt"     
ElecActive = np.loadtxt("ElecActive.txt", delimiter=',')  #, usecols=(0, 11), unpack=True)

#Case monopolar: 
index = np.nonzero(ElecActive)
ind = np.array(index)
nelectrodes = np.size(np.nonzero(ElecActive))
BC = np.arange(3,15)
Body = np.arange(7,19)
Body = np.flipud(Body)
n_source = np.arange(1,13)

with open("CASE.txt") as f:
    with open("case.sif", "w") as f1:
        for line in f:
            f1.write(line)
            
with open("TIMESTEP.txt") as g:
    with open("MyTimestep.f90", "w") as g1:
        for line in g:
            g1.write(line)            
            
with open("case.sif", "a") as f:
    for i in range(nelectrodes):
        f.write("Boundary Condition "+str(BC[i])+"\n")
        f.write("  Target Boundaries(1) = "+str(Body[ind[0,i]])+"\n")
        f.write("  Name = \"SourceE"+str(n_source[ind[0,i]])+"\""+"\n")
        f.write("  Current Density BC = True"+"\n")
        f.write("  Current Density = Variable Time; Real Procedure \"MyTimestep\" \"NeuralEncodingE"+str(n_source[ind[0,i]])+"\""+"\n")        
        f.write("end"+"\n"+"\n")
        
with open("MyTimestep.f90", "a") as g:
    for i in range(nelectrodes):
        nEl =str(n_source[ind[0,i]])
        g.write("FUNCTION NeuralEncodingE"+str(nEl)+"( Model, n, T) RESULT(s)"+"\n")
        g.write("    USE DefUtils"+"\n"+"    USE Lists"+"\n"+"    IMPLICIT None"+"\n"+"\n")
        g.write("    TYPE(Model_t) :: Model"+"\n")
        g.write("    TYPE(Solver_t) :: Solver"+"\n")
        g.write("    INTEGER :: n , TS,  i "+"\n"+"\n")
        
        g.write("    REAL(KIND=dp) :: s, T, dt, d_t"+"\n")
        g.write("    REAL(KIND=dp), ALLOCATABLE,DIMENSION(:,:) :: X"+"\n")
        g.write("    LOGICAL :: Transient"+"\n"+"\n")
        g.write("    CALL GetTS(Model,Solver,dt, Transient,TS)"+"\n")
        g.write("    CALL Getd_t(Model,Solver,dt,Transient,d_t)"+"\n"+"\n")
        
        g.write("    ALLOCATE(X(12,TS))"+"\n"+"\n")
        g.write("    OPEN(unit=10,file="+"\""+filename+"\""+" ,STATUS=\"OLD\",ACTION=\"READ\")"+"\n")       
        g.write("    DO i = 1,TS"+"\n")
        g.write("    READ(10,*) X(1,i),X(2,i),X(3,i),X(4,i),X(5,i),X(6,i),X(7,i),X(8,i),X(9,i),X(10,i),X(11,i),X(12,i)"+"\n")
        g.write("    END DO"+"\n")
        g.write("    CLOSE(10)"+"\n"+"\n")
        g.write("    i=1"+"\n")
        g.write("    DO WHILE (i <= TS)"+"\n")
        g.write("        IF (i*d_t == T) THEN"+"\n")
        g.write("            s = X("+str(nEl)+",i)"+"\n")
        g.write("        END IF"+"\n")
        g.write("    i = i+1"+"\n")      
        g.write("    END DO"+"\n")
        g.write("    DEALLOCATE(X)"+"\n")
        g.write("END FUNCTION NeuralEncodingE"+str(nEl)+"\n"+"\n")
        print("Electrode: "+str(nEl)+" will be activated in this simulation")
        
        
