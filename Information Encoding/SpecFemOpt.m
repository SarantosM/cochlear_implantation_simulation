function [nfreq,NFFT,nframes,d_t,wnd,nbins,idx_nfft] = SpecFemOpt(x,fs,BWElec,wnd_max,nframes_max,f_ovr)
prc = 4;

% [wlen_min,t_wnd_min,nfreq,nbins,binloss,nfft_min,nframes,d_t] = SpecFEM_nfft_min(x,fs,BWElec);
%binloss = num2cell(binloss);
N = length(x);
tend = N/fs;
%exponent = ceil(log(wlen_min)/log(2));
nfft_min = ceil(fs/BWElec(1,1));     %Lowest frequency of Electrode Array
            %[s]
nfft_max = ceil(wnd_max*fs);


NFFT = nfft_min:1:nfft_max;

for i = 1:length(NFFT)
    noverlap(i) = NFFT(i)*f_ovr; 
    nfreq(i) = ceil((1+NFFT(i))/2); % calculate the number of unique fft points
    nframes(i)= 1+fix((N-NFFT(i))/ceil((1-f_ovr)*NFFT(i))); % calculate the number of signal frames
   %nframes= 1+fix((N-wlen_min)/ceil((1-f_ovr)*NFFT(i)));
    d_t = tend/nframes(i);
    wnd(i) = NFFT(i)/fs;
    F = linspace(0,fs/2,nfreq(i));
    %[Notes,dist] = findnotes(F,1);
    %A(i) = length(Notes(dist < 2)))

    for j = 1:12
        el_freq_index{j} = find(F >= BWElec(j,1) & F <= BWElec(j,2));
        nbins{i,j} = length(el_freq_index{j});
    end
    
end
nbins = cell2mat(nbins);
idx_nfft = find(nframes < nframes_max);   %max(sum(nbins(:,1:2),2)) & && max(A)
% disp(['The nfft with most spectral content in the first 6 Electrodes is ',...
%     num2str(NFFT(idx_nfft))])

% subplot(2,2,1)
% % % title('Time-Frequency Tradeoff')
% % % semilogy(NFFT,nframes);
% % % hold on; 
% % % semilogy(NFFT,nfreq)
% % % legend(['nframes'],['nfreq'])

% % % figure;
% % % plot3(wnd*1e3,nfreq,nframes);
% figure;
% % subplot(2,2,1)
% semilogy(NFFT(1:end-1),abs(diff(nframes)));
% hold on; 
% semilogy(NFFT(1:end-1),abs(diff(nfreq)));
% legend(['nframes'],['nfreq'])

% binBW = mean(diff(F));
%[idx] = find(max(diff(nframes)) & max(diff(nfreq)));

% %nfft = 2*nfreq - 2;
% F = linspace(0,fs/2,nfreq);
% 
% binBW = mean(diff(F));
% 
% for i = 1:12
%     if i == 12
%         el_freq_index{i} = find(F >= BWElec(i,1) & F <= BWElec(i,2));    
%     
%         binloss{i} = 1 - length(el_freq_index{i})/length(BWElec(i,1):prc:BWElec(i,2));
%     else
%         el_freq_index{i} = find(F >= BWElec(i,1) & F < BWElec(i,2));    
%     
%         binloss{i} = 1 - length(el_freq_index{i})/length(BWElec(i,1):prc:BWElec(i,2));
%     end  
%     nbins{i} = length(el_freq_index{i});
% end
% 
% 
% 
% 

    
    
    
    
    