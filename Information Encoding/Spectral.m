function [NF_m,NF] = Spectral(x,fs,BWElec,fcentral,f_ovr,wnd_max,nframes_max,fig,cmap)
get(0,'Factory');
set(0,'defaultfigurecolor',[1 1 1]);
N = length(x);
tend = N/fs;
t = (0:N-1)/fs;

%%%%%%%-----Analysis------%%%%%%%
%Electrode Specifications
fc = [149,261,408,601,854,1191,1638,2233,3028,4090,5510,7412];

BWElec = [70 170;170 300;300 469;469 690;690 982;982 1368;1368 1881;...
    1881 2564;2564 3475;3475 4693;4693 6320;6320 8500];

fres = fs/N; %[Hz/Bin]

if fres < 20
    disp(['Frequency resolution is ',num2str(fres),...
        ' [Hz/Bin]. Signal can contain all auditory spectrum'])
else
    disp(['Frequency resolution is ',num2str(fres),...
        ' [Hz/Bin]. Warning!The sample size must be larger than'...
        ,num2str(Nmin),' samples'])
end
    
%%%%%%%%-------Frequency Domain--------%%%%%%%%%
%FFT
% for i = 1:size(BWElec,1)
%     figure(i)
%     [freq{1,i},X{1,i},f0{1,i}] = plotfft(x,fs,BWElec(1,:));
% end

%%%%%%%%-------------MIN-NFFT----------------------------%%%%%%%%%%%%%%%%%%%
%Finds minimum window bounded by the frequency resolution (fs/N) and
%calculates the minimum nfft with noverlap = nfft
NF_m = {};
[wlen_min,t_wnd_min,NF_m.nfreq,NF_m.nbins,NF_m.binloss,NF_m.nfft_min,NF_m.nframes,NF_m.d_t] = SpecFEM_nfft_min(x,fs,BWElec,f_ovr);

[NF_m.X, NF_m.F, NF_m.T, NF_m.P] = spectrogram(x, hamming(NF_m.nfft_min,'periodic'), round(NF_m.nfft_min*f_ovr), NF_m.nfft_min, fs,'onesided');
if size(NF_m.P,1) < 12
    print('Rows are less than 12')
end

%%%%%--------Spectrogram--------------------------
%XdB = 10*log10();    %/max(abs(NF_m.P))
NF_m.PdB = 10*log10(abs(NF_m.P)./max(abs(NF_m.P)));
figure
imagesc(NF_m.T*1e3,(NF_m.F),flipud(NF_m.PdB))%,clipvals);
xlabel('Time (ms)');
ylabel('Freq (Hz)');
colorbar;
hc=colorbar;
title(hc,'dB');
colormap(cmap);
title(['Spectrogram at nfft-min: nframes = ',num2str(NF_m.nframes),', nfreq = ',num2str(NF_m.nfreq),'  dt = ',num2str(mean(diff(NF_m.T))*1e3),' [ms], novrlp = ',num2str(f_ovr*100),'%']);
hax = gca;
hax.YTickLabel{end+1} = fs/2;
hax.YTickLabel = flipud(hax.YTickLabel);

%-------Average per Electrode
[NF_m.Pelec,NF_m.nbinsE,NF_m.Pelec_dB ] = ElecPowerMean(NF_m.F,NF_m.P,NF_m.T,BWElec,cmap);


%%%%%%%%%%%%%%----------SPEC FEM OPT --------------%%%%%%%%%%%%%%%%%%%%%%%%
%wnd_max = 0.3;
NF = {};
[NF.nfreq,NF.NFFT,NF.nframes,NF.d_t,NF.wnd,NF.nbins,NF.idx_nfft] = SpecFemOpt(x,fs,BWElec,wnd_max,nframes_max,f_ovr);
NF.nfft = NF.NFFT(min((NF.idx_nfft)));        % 1024; %64;
NF.noverlap = round(NF.nfft*f_ovr);      %nfft/4;
                        %window = hamming(nfft);
wnd= hamming(NF.nfft,'periodic');
                   
%plot3(wnd_l/fs*1e3,nfreq,nframes);

%nfft = wnd_l(end);
[NF.X, NF.F, NF.T, NF.P] = spectrogram (x, hamming(NF.nfft,'periodic'), NF.noverlap, NF.nfft, fs,'onesided');
if size(NF.P,1) < 12
    print('Rows are less than 12')
end
nframes1 = 1+fix((N-NF.nfft)/round((1-f_ovr)*NF.nfft));
%nframes1 = NF.nframes(min(NF.idx_nfft));
nfreq1 = NF.nfreq(min(NF.idx_nfft));
wnd1 = NF.wnd(min(NF.idx_nfft));
d_t = mean(diff(NF.T));
%%%%%--------Spectrogram
%XdB = 10*log10(abs(NF.X));
NF.PdB = 10*log10(abs(NF.P)./max(abs(NF.P)));
figure
imagesc(NF.T*1e3,(NF.F),flipud(NF.PdB))%,clipvals);
xlabel('Time (ms)');
ylabel('Freq (Hz)');
colorbar;
hc=colorbar;
title(hc,'dB');
colormap(cmap);
title(['Spectrogram: nframes = ',num2str(nframes1),', nfreq = ',num2str(nfreq1),' dt = ',num2str(d_t*1e3),' [ms], novrlp = ',num2str(f_ovr*100),'%']);
ylim([0 NF.F(end)]);
hax = gca;
hax.YTickLabel{end+1} = fs/2;
hax.YTickLabel = flipud(hax.YTickLabel);

[NF.Pelec,NF.nbinsE,NF.Pelec_dB] = ElecPowerMean(NF.F,NF.P,NF.T,BWElec,cmap);

figure
nb = cell2mat(NF_m.nbins);
plot(nb);
hold on;
nc = cell2mat(NF.nbinsE);
plot(nc);
hold on; 
plot(nb,'ro'); hold on; plot(nc,'ro')
for i = 1:12
    elec_name{i} = strcat('E',num2str(i));
end
elec_place = 1:12;
xticks(elec_place)
xticklabels(elec_name)
xlabel('time [ms]');
title(['# freq-bins/Electrode BW']) %at twnd = ',num2str(min(NF.wnd(NF.idx_nfft))*1e3),' ms'])   %
xlabel('Electrode'); ylabel('number of Frequency Bins')
legend(['twnd = ',num2str(t_wnd_min*1e3),' ms'],['twnd = ',num2str(min(NF.wnd(NF.idx_nfft))*1e3),' ms'])

if fig == 0
    close all
end

% % % figure
% % % for i = 1:12
% % %     hold on;
% % %     plot(wnd_l/fs*1e3,KK(:,i));
% % %     title('Window Size vs # of frequency Bins per Electrode BW')
% % %     Legend{i} = strcat('E',num2str(i));
% % %     xlabel('wnd [ms]'); ylabel('# of bins')
% % % end
% % % legend(Legend);



% wlen_min = ceil(fs/BWElec(1,1));     %Lowest frequency of Electrode Array
% t_wnd_min = wlen_min/fs;


% figure
% nb = cell2mat(NF_m.nbins);
% plot(nb);
% hold on; plot(nb,'ro')
% for i = 1:12
%     elec_name{i} = strcat('E',num2str(i));
% end
% elec_place = 1:12;
% xticks(elec_place)
% xticklabels(elec_name)
% xlabel('time [ms]');
% title(['# of freq-bins per Electrode BW at nfft-min: twnd = ',num2str(t_wnd_min*1e3),' ms'])  %
% xlabel('Electrode'); ylabel('number of Frequency Bins')

