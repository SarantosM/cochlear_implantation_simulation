%function [x,time,fs,TS,d_t] = InformationEncoding(audio_filename,nEl,TS_max,time_range,c,MPT)
clear all
clc
close all
get(0,'Factory');
set(0,'defaultfigurecolor',[1 1 1]);
audio_filename = 'RG1.wav'
myPath = pwd
%myPath = "C:/home/saradis/Documents/sarantos_cmear_research"; %/InformationEncoding"                 %Linux 
%cd(myPath)
addpath(genpath(pwd))

%%%%%%%%%%%-----------Electrode Characteristics------%%%%%%%%%%%%%%%%%%%%%
%%% Central Frequencies and Bandwidths --- Find Corresponding Musical Notes

fcentral = [149,261,408,601,854,1191,1638,2233,3028,4090,5510,7412];
BWElec = [70 170;170 300;300 469;469 690;690 982;982 1368;1368 1881;...
    1881 2564;2564 3475;3475 4693;4693 6320;6320 8499];
[fc_notes,BW_notes,freq_notes,text_notes] = findElec_notes(fcentral,BWElec);
%ERB = 24.7*(0.00437*fcentral+1); 
%ERB(2,:) = diff(BWElec',1);

%%%%%%%%%%%-----------Read Audio---------------------%%%%%%%%%%%%%%%%%%%%%

%[] = AudioSeg(myPath,audio_filename,trange,new_name)
audio = [];
[audio.x,audio.fs] = audioread(strcat(myPath,"/INPUT/Audio/",audio_filename));

audio.x = audio.x ./max(abs(audio.x));         %Normalize signal
disp(['The sample is ',num2str(length(audio.x)/audio.fs),' seconds, fs = ',num2str(audio.fs),' Hz'])
soundsc(audio.x,audio.fs);
audio.t = (0:length(audio.x)-1)/audio.fs;
%%%-------------------------Downsampling---------------------%%%%%%%%%%%%
if audio.fs > 17200 %2 * maxFreq  %maxFreq = 8500;
    disp('Input audio is downsampled to 17200 ')
    x = resample(audio.x,17200,audio.fs);
    fs = 17200;
end

N = length(x);
tend_a = N/fs;
time = (0:N-1)/fs;
soundsc(x,fs);

plot(audio.t,audio.x,'b')
hold on; plot(time,x,'ro')
[CHORD] = chord_recognition(x,fs);

%%%%%%%%%%%%%%%%%%----Time-Frequency Analysis-------%%%%%%%%%%%%%%%%%%
figure;
%subplot(211)
plot(time*1e3,x);
xlabel('time [ms]');ylabel('Amplitude')
title(['Input Audio: fs = ',num2str(fs),' Hz'])
%subplot(212)
plotfft(x,fs,[70 8500]);


wnd_max = 0.3; nframes_max = 70;
fig = 1; f_ovr = 0.9; cmap = magma();

[NF_m,NF] = Spectral(x,fs,BWElec,fcentral,f_ovr,wnd_max,nframes_max,fig,cmap);

disp(['The min Power is ',num2str(min(min(NF_m.PdB))),' dB and the Mean Power is ',num2str(mean(mean(NF_m.PdB))),' dB']);

x = x(round(0.2*fs):end);
N = length(x);
tend_a = N/fs;
time = (0:N-1)/fs;

%%%---------------------BandPassing at 17200 Hz------------%%%%%%%%%%%%%%%%
[BP_g] = Gammatone(x,fs,fcentral,BWElec);
BP = Bands(x,fs,BWElec);
plotBands(BP',time,(['BandPassing at 12 filter Banks: fs = ',num2str(fs),' Hz']),'b');
%hold on;
%figure
plotBands(BP_g',time,(['Gammatone at 12 filter Banks: fs = ',num2str(fs),' Hz']),'k');

% y = BP(:,1);
% for i = 1:12
%     y(end + zeros(round(0.2*fs),1): i*) 
% % a = 0.2;
% % y = [BP(:,1); zeros(round(a*fs),1); BP(:,2); zeros(round(a*fs),1);BP(:,3); zeros(round(a*fs),1);BP(:,4); zeros(round(a*fs),1);...
% %     BP(:,5); zeros(round(a*fs),1);BP(:,6); zeros(round(a*fs),1);BP(:,7); zeros(round(a*fs),1);BP(:,8); zeros(round(a*fs),1);BP(:,9); zeros(round(a*fs),1);...
% %     BP(:,10); zeros(round(a*fs),1);BP(:,11); zeros(round(a*fs),1);BP(:,12); zeros(round(a*fs),1)];
% % audiowrite(strcat(pwd,"/INPUT/Audio/",'RG_17200_BP.wav'),y,fs);
%%%%%%%-------n-of-M Strategy----------------------%%%%%%%%%%
MPT = - 20;     %Minimum Perceptual Threshold
[ElecActive] = find(mean(NF_m.Pelec_dB,2) > MPT & mean(NF.Pelec_dB,2) > MPT);
disp(['-------------------N-of-M Strategy---------------------- '])
disp(['Electrodes that have higher power than MPT = ',num2str(MPT),' dB'])
disp(['Electrodes: ',num2str(ElecActive'),' will be active'])
BW = [BWElec(ElecActive(1),1) BWElec(ElecActive(end),2)];
disp(['The Encoded BW is ',num2str(BW),' Hz'])

% % % for i = 1:12
% % %     [BW_99(i) ,Flo(i), Fhi(i), POWER(i)] = obw(x,fs,BWElec(i,:));
% % % end
% % % POWERdB = 10*log10(POWER);
% nEl = 5;
% [powsort idx] = sort(POWER,'descend');
% ElecActive = sort(idx(1:nEl));

for i = 1:length(ElecActive)
    [CHORD2{i}] = chord_recognition(BP(:,ElecActive(i)),fs);
end
%%%----HILBERT TRANSFOM, Envelope Detection and Fine Structure-------%%%%%%%

signalx = zeros(length(x),1);
for i = 1:length(ElecActive)
    hx_BP(:,i) = myhilbert(BP(:,i),fs,time);
    
    signalx = signalx + BP(:,i);
end

soundsc(signalx,fs)
%%audiowrite(strcat(pwd,"/INPUT/Audio/",'RG_17200_signalx.wav'),signalx,fs);


 
% %%%%-------FEM parameter Optimization Algorithm-------%%%%%%%
TS_max = 250; tmin = 100e-3; tend = 0.5; c = 2;
time_range = [tmin tend];
[TS,d_t,tend,Fmax,Fmin] = findBestd_t(fs,TS_max,time_range,BW,c);

d_t_fem = mean(d_t{1,c})
fs_fem = round(1./d_t_fem)
d_t_fem = 1/fs_fem;
d_t_fem = round(d_t_fem,7)

time_fem = (0:TS-1)/fs_fem;
tend_fem = TS*d_t_fem
disp(['----------FEM parameter Optimization Algorithm------'])
disp(['The chosen physical time is ',num2str(TS*1/fs_fem*1e3),' ms with a dt_fem '...
    ,num2str(1/fs_fem*1e3),' ms, fs_fem = ',num2str(fs_fem),' Hz'])

%max(max(abs(angle(hx_BP))));
% [NF_m_fem,NF_fem] = Spectral(signalx(1:TS),fs_fem,BWElec,fcentral,0.1,10,fig);

%%%%----------Fine Structure and Envelope------------------%%%%%%%%%%%%%
for i = 1:length(ElecActive)
    FineStr(:,i) = angle(hx_BP(:,i))./max(max(abs(angle(hx_BP))));
    Envelope(:,i) = abs(hx_BP(:,i))./ max(max(abs(abs(hx_BP))));
    output(:,i) = resample(FineStr(:,i),fs_fem,fs); 
end

%plotBands(output(1:TS,:)',time_fem,(['FineStructure at 4 filter Banks: fs = ',num2str(fs_fem),' Hz']),'b');
% % y = [FineStr(:,1); zeros(round(a*fs),1); FineStr(:,2); zeros(round(a*fs),1);FineStr(:,3); zeros(round(a*fs),1);FineStr(:,4)]; 
% % audiowrite(strcat(pwd,"/INPUT/Audio/",'RG_17200_FineStr.wav'),y,fs);
x_fem = resample(signalx,fs_fem,fs);
% % audiowrite(strcat(pwd,"/INPUT/Audio/",'RG_2005_1234.wav'),x_fem,fs_fem);
% % 
% % audiowrite(strcat(pwd,"/INPUT/Audio/",'RG_FEM_2005_1234.wav'),x_fem(1:TS),fs_fem);
% y = [Envelope(:,1); zeros(round(a*fs),1); Envelope(:,2); zeros(round(a*fs),1);Envelope(:,3); zeros(round(a*fs),1);Envelope(:,4)];
% soundsc(y,fs)
Input_fem = sum(output,2);

plot(time(1:round(tend_fem*fs))*1e3,x(1:round(tend_fem*fs)),'b'); hold on;
plot(time_fem*1e3,x_fem(1:TS),'ro');
xlabel('time [ms]');ylabel('Amplitude')
legend(['fs = ',num2str(fs),' [Hz]'],['fsFEM = ',num2str(fs_fem),' [Hz]'])
title(['Time series Before and after downsampling TS:',num2str(TS),' BW:',num2str(BW),'Hz']);
%plotBands(output',time_fem,(['Gammatone at 12 filter Banks: fs = ',num2str(fs),' Hz']),'k');
 SA = 4*pi*(0.06*1e-3)^2;
outputJ = output(1:TS,:).*(1e-3/SA);

%[idx] = find(outputJ == 0);
%soundsc(y,fs_fem);

JElec = zeros(TS,12);
for i = 1:length(ElecActive)
    JElec(:,ElecActive(i)) = outputJ(:,i);
end

plotBands(JElec',time_fem,(['FineStructure Banks: fs = ',num2str(fs_fem),' Hz']),'b');
O = find(JElec(:,1:4) == 0);
%%%%%%%%%%%%%-----------Export Files for FEM ---------%%%%%%%%%%%%%%%%%

% stimuli = 'stimuli.txt'
% fid = fopen(stimuli, 'wt');
% fprintf(fid, [repmat('%g,', 1, size(JElec,2)-1) '%g\n'], JElec.');
% fclose(fid);

% info = [TS d_t_fem];
% fid = fopen('info.txt', 'wt');
% fprintf(fid, [repmat('%g,', 1, size(info,2)-1) '%g\n'], info.');
% fclose(fid);

% ElecActive2 = zeros(1,12);
% for i = 1:length(ElecActive)
%     ElecActive2(ElecActive(i)) = ElecActive(i);
% end

% fid = fopen('ElecActive.txt', 'wt');
% fprintf(fid, [repmat('%g,', 1, size(ElecActive2,2)-1) '%g\n'], ElecActive2.');
% fclose(fid);

% save(strcat('InfEnc',audio_filename(1:3),'.mat'));

% tend_fem = TS * d_t_fem
% time_fem = 0:1/fs_fem:tend_fem;
% d_t_fem = 1/fs_fem;
% soundsc(y(1:TS),fs_fem)
 
% figure;

% figure;
% % subplot(211)
% plotfft(x,fs,BW);
% % subplot(221)
% figure;
% plotfft(y(1:TS),fs_fem,BW);
% %legend(['gammatone'],['bandpass'])

% %%%%------ BANDS -----------------------%%%%%%%%%
% % 
% [gammabands_fs] = Gammatone(x,fs,fcentral,BWElec);
% [passbands_fs] = Bands(x,fs,BWElec);

% % %a = 1;
% % figure;
% % plotfft(gammabands(:,a),fs,[70 2*fs]);
% % hold on; plotfft(passbands(:,a),fs,[70 2*fs]);
% % legend(['gammatone'],['bandpass'])

