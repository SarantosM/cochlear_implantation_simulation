function [swave] = tonegen(a,f,tend,fs,figure)
%A :row vector of amplitudes
%f :row vector of frequencies in Hz
%A and f must be same length
%t : time vector
%fs : Sampling frequency
%figure = 1 Displays figure Else no figure
%a=[10 2 5 8]; f=[100 250 440 4000];

%t = time;
%n = N;
%n = length(t);
N = tend*fs;
t = (0:N-1)/fs;
swave = zeros(size(t));
for i=1:length(a)
swave = swave + a(i)*sin(2*pi*f(i)*t); %+ 0.5*mean(a)*rand(1,n);
end
% Fourier transform
swaveX = fft(swave)/N;
hz = linspace(0,fs/2,floor(N/2)+1);
% plot
if figure == 1
    subplot(211), plot(t*1e3,swave)
    xlabel('Time (ms)'), ylabel('amplitude')
    subplot(212)
    plot(hz,2*abs(swaveX(1:length(hz))))
    set(gca,'xlim',[0 max(f)*1.3]);
    xlabel('Frequencies (Hz)'), ylabel('amplitude')
    soundsc(swave,fs);
else %if figure == 'off'
    return
    %end
end


% N = length(x);
% t = 0:1/(Fs):tend;     %Generate time vector of DT = 1/Fs;
% N = length(t);
% A = 1;
% w_C4 = 2*pi*261.63;
% w_E4 = 2*pi*329.63;
% w_G4 = 2*pi*392.00;
% 
% x_C4 = real(A*exp(-j*w_C4*t));
% x_E4 = real(A*exp(-j*w_E4*t));
% x_G4 = real(A*exp(-j*w_G4*t));
% 
% f = k*Fs/N ;
% 
% %format rat
% %just = [1 16/15 9/8 6/5 5/4 4/3 7/5 3/2 8/5 5/3 7/4 15/8 2]'
% format short
% sigma = 2^(1/12);
% k = (0:12)';
% equal = sigma.^k;
% num = [1 16 9 6 5 4 7 3 8 5 7 15 2]';
% den = [1 15 8 5 4 3 5 2 5 3 4  8 1]';
% just = num./den;
% delta = (equal - just)./equal;
% T = [k equal num den just delta];
% fprintf('   k       equal           just              delta\n')
% fprintf('%4d %12.6f %7d/%d %11.6f %10.4f\n',T')
% f = 987.77*equal;
% w = 2*pi*f;
% A = 1;
% x(length(t),length(f)) = real(A*exp(-j*t'.*w'));


