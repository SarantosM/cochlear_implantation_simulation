function [swave,f] = chordgen(a,Notes,t,fs,figure)
%A :row vector of amplitudes
%f :row vector of frequencies in Hz
%A and f must be same length
%t : time vector
%fs : Sampling frequency
%figure = 1 Displays figure Else no figure
%a=[10 2 5 8]; f=[100 250 440 4000];

%t = time;
%n = N;
n = length(t);
swave = zeros(size(t));

% % [ndata, text_notes] = xlsread('musicfreq.xlsx');
% % freq_notes = ndata(:,1);
text_notes = {'C0';' C#0/Db0 ';'D0';' D#0/Eb0 ';'E0';'F0';' F#0/Gb0 ';'G0'...
    ;' G#0/Ab0 ';'A0';' A#0/Bb0 ';'B0';'C1';' C#1/Db1 ';'D1';' D#1/Eb1 ';...
    'E1';'F1';' F#1/Gb1 ';'G1';' G#1/Ab1 ';'A1';' A#1/Bb1 ';'B1';'C2';...
    ' C#2/Db2 ';'D2';' D#2/Eb2 ';'E2';'F2';' F#2/Gb2 ';'G2';' G#2/Ab2 ';...
    'A2';' A#2/Bb2 ';'B2';'C3';' C#3/Db3 ';'D3';' D#3/Eb3 ';'E3';'F3';...
    ' F#3/Gb3 ';'G3';' G#3/Ab3 ';'A3';' A#3/Bb3 ';'B3';'C4';' C#4/Db4 ';...
    'D4';' D#4/Eb4 ';'E4';'F4';' F#4/Gb4 ';'G4';' G#4/Ab4 ';'A4';...
    'A#4/Bb4 ';'B4';'C5';' C#5/Db5 ';'D5';' D#5/Eb5 ';'E5';'F5';...
    ' F#5/Gb5 ';'G5';' G#5/Ab5 ';'A5';' A#5/Bb5 ';'B5';'C6';' C#6/Db6 ';...
    'D6';' D#6/Eb6 ';'E6';'F6';' F#6/Gb6 ';'G6';' G#6/Ab6 ';'A6';' A#6/Bb6 ';...
    'B6';'C7';' C#7/Db7 ';'D7';' D#7/Eb7 ';'E7';'F7';' F#7/Gb7 ';'G7';...
    ' G#7/Ab7 ';'A7';' A#7/Bb7 ';'B7';'C8';' C#8/Db8 ';'D8';' D#8/Eb8 ';...
    'E8';'F8';' F#8/Gb8 ';'G8';' G#8/Ab8 ';'A8';' A#8/Bb8 ';'B8'};
for i = 1:length(text_notes)
    freq_notes(i,1) = 440*2^((-58+i)/12);
end
for i = 1:length(Notes)
    dummy = strfind(text_notes, Notes{i});
    Index{i} = find(~cellfun(@isempty,dummy));
    f(i) = freq_notes(Index{i});
end
for i=1:length(a)
 
swave = swave + a(i)*sin(2*pi*f(i)*t); %+ 0.5*mean(a)*rand(1,n);
end
% Fourier transform
swaveX = fft(swave)/n;
hz = linspace(0,fs/2,floor(n/2)+1);
% plot
if figure == 1
    subplot(211), plot(t*1e3,swave)
    xlabel('Time (ms)'), ylabel('amplitude')
    subplot(212)
    plot(hz,2*abs(swaveX(1:length(hz))))
    set(gca,'xlim',[0 max(f)*1.3]);
    xlabel('Frequencies (Hz)'), ylabel('amplitude')
    soundsc(swave,fs);
else %if figure == 'off'
    return
    %end
end

