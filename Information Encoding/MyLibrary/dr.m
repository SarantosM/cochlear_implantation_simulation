%Delay Reconstruction
function [] = dr(x,tau,c)
%tau = 1500;
N = length(x);
figure
if c == 1;
plot3(x(2*tau+1:end),x(tau+1:end-tau),x(1:end-2*tau))
title(['Delay Reconstruction: tau = ',num2str(tau)]);
xlabel('x(t)'); ylabel('x(t-tau)'); zlabel('x(t-2tau)');
else 
    figure
    handle = axes('Parent', figure());
    hold(handle, 'off');
    for tau = 1:N-1 %round(N/2)
%         plot3(x(2*tau+1:end),x(tau+1:end-tau),x(1:end-2*tau))
%         title(['Delay Reconstruction: tau = ',num2str(tau)]);
%         xlabel('x(t)'); ylabel('x(t-tau)'); zlabel('x(t-2tau)');
        plot(x(1:end-tau),x(tau+1:end),'b')
        title(['Delay Reconstruction: tau = ',num2str(tau)]);
        drawnow
        pause(0.1)
%         w = waitforbuttonpress;
%         f = figure(tau);
%         kkey = get(f,'CurrentCharacter');
%         switch kkey
%             case 'p'
%                 pause on
%         end

    end
end
end

