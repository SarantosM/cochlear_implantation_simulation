function [] = plotBands(BP,time,titl,cg)
num_channels = size(BP,1);
for i = 1:num_channels
    elec_name{i} = strcat('E',num2str(i));
end
factor = max(max(abs(BP)));
%elec_place = 1:12*factor;
% for i = 1:num_channels
%     EEG(i,:) = EEG(i,:)/(max(abs(EEG(i,:))));
% end
%d = max(max(abs(EEG(11,:))));
%f = figure(1);
%hold on;
figure;
hold on;
for i = 1:num_channels
    %factor = max(abs(EEG(i,:)))*1.2;
    plot(time*1e3,BP(i,:)+ factor*i,cg)
    elec_place(i) = mean(BP(i,:) + factor*i);
end


for i = 1:num_channels
    elec_name{i} = strcat('E',num2str(i));
end

yticks(elec_place)
yticklabels(elec_name)
xlabel('time [ms]');
title(titl)
