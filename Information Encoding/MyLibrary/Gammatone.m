function [output] = Gammatone(x,fs,fc,BW)


% for i = 1:length(fc)
%     cfArray{i} = ERBSpace(BW(i,1), BW(i,2), 1);
% end

lowFreq = BW(1,1);
minBW = min(BW(:,2)-BW(:,1));
fcoefs=MakeERBFilters(fs,fc,lowFreq,minBW);
output = ERBFilterBank(x, fcoefs);
output = output';