function [y] = stats(x)
[N,C] = size(x);
y = [mean(x); max(x); min(x); var(x)];


