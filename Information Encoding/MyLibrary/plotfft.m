function [freq,X,f0] = plotfft(x,fs,BW)

%BW = [70 8500];

% % Fourier transform
N = length(x);
X = fft(x)/N;
%hannwin = 1.01*max(x)*(.5*(1-cos(2*pi*linspace(0,1,N))));     %.5*(1-cos(2*pi*linspace(0,1,N)));
%X2 = fft(x.*hannwin)/N;
freq = linspace(0,fs/2,floor(N/2)+1);
f = find(freq > BW(1) & freq <BW(2));
a = find(abs(X(f))== max(abs(X(f))));
f0 = round(freq(f(a)));

%figure
Xp = X.*conj(X);
dB = 10*log10(abs(Xp)./max(Xp));
subplot(211)
plot(freq,(dB(1:length(freq))))
%hold on;
%plot(freq,2*abs(X2(1:length(freq))),'r-')
xlabel('Frequencies (Hz)'), ylabel('dB')
title(['FFT F_0 = ',num2str(f0),'Hz, BW = [',num2str(BW(1)),' - ',num2str(BW(2)),'] Hz'])
%legend(['Original'],['Hanned'])
%set(gca,'xlim',[0 max(f)*1.3]);
subplot(212)
plot(freq,2*abs(X(1:length(freq))))
xlabel('Frequencies (Hz)'), ylabel('FFT Amplitude')



%bar(freq,2*abs(X(1:length(freq))))
% 
% subplot(212)
% time = 0:1/fs:N/fs;
% time = time(1:end-1);
% plot(time.*1e3,x)
% hold on;
% plot(time.*1e3,hannwin,'r-')
%a = find(abs(X) == max(abs(X)));
% x = x(:)
% n = nextpow2(length(x));
% N = 2^n;
% if N >= length(x)
%     x = [x; zeros(N-length(x),1)];
%     
% else
%     N = length(x);
% end
% % N = length(x);
% % if mod(N,2) == 1
% %      %x(length(x)+1) = 0;
% %      x = x(1:end-1);
% % else
% %      x = x;
% % end

% N = length(x);
% X = fft(x)/N;
% X = X(1:N/2+1);
% freq = (0:fs/N:fs/2)';
% figure
% Xn = abs(X)/max(abs(X));
% bar(freq,abs(X)/max(abs(X)))
% f = find(freq > BW(1) & freq <BW(2));
% a = find(abs(X(f))== max(abs(X(f))));
% f0 = round(freq(f(a)));
% %a = find(abs(X) == max(abs(X)));
% %title(['FFT fmax = ',num2str(freq(a)),'[Hz]'])
% title(['FFT F_0 = ',num2str(f0),'[Hz]'])
% xlabel('frequency [Hz]')
% ylabel('FFT Magnitude')