function [output] = Bands(x,fs,BW);

N = length(x);
output = zeros(N,length(BW));
for i = 1:length(BW)
    output(:,i) = bandpass(x,BW(i,:),fs);
end

%output = out