function [Pelec,nbins,Pelec_dB ] = ElecPowerMean(F,P,T,freqRange,cmap);


Pelec = zeros(12,size(P,2));
for i = 1:12
    if i == 12
        [el_freq_index{i}] = find(F >= freqRange(i,1) & F <= freqRange(i,2));    
        Pelec(i,:) = sum(P(el_freq_index{i},:),1)/length(el_freq_index{i});
    else
        [el_freq_index{i}] = find(F >= freqRange(i,1) & F < freqRange(i,2));    
        Pelec(i,:) = sum(P(el_freq_index{i},:),1)/length(el_freq_index{i});
    end
    nbins{i} = length(el_freq_index{i});
end

% for i = 1:12
%     
% end

for i = 1:12
    elec_name{i} = strcat('E',num2str(i));
end
elec_name = fliplr(elec_name);
nframes = size(P,2);
dt = mean(diff(T))
%XdB = 10*log10(abs(Pelec));
Pelec_dB = 10*log10(abs(Pelec)./max(abs(Pelec)));
figure
imagesc(T*1e3,fliplr(1:12),Pelec_dB)%,clipvals);
xlabel('Time (ms)');
ylabel('Electrode Channels');
colorbar;
hc=colorbar;
title(hc,'dB');
colormap(cmap);
title(['Spectrogram with Averaged Power per BW: nframes = ',num2str(nframes), ' dt = ',num2str(dt*1e3),' [ms]']);
yticks(1:12)
yticklabels(elec_name)
xlabel('time [ms]');


A = isnan(Pelec);
if max(max(A)) == 1;
    %disp('Your matrix has NaN')

    
    for i = 1:12
        if max(max(isempty(el_freq_index{i}))) == 1;
        disp(['Frequency bin at Electrode: ',num2str(i),' is empty'])
        end
    end
else
    return
end
% % %     el_freq_index{1} = find(F >= freqRange(1,1) & F < freqRange(1,2));
% % %     el_freq_index{2} = find(F >= freqRange(2,1) & F < freqRange(2,2));
% % %     el_freq_index{3} = find(F >= freqRange(3,1) & F < freqRange(3,2));
% % %     el_freq_index{4} = find(F >= freqRange(4,1) & F < freqRange(4,2));
% % %     el_freq_index{5} = find(F >= freqRange(5,1) & F < freqRange(5,2));
% % %     el_freq_index{6} = find(F >= freqRange(6,1) & F < freqRange(6,2));
% % %     el_freq_index{7} = find(F >= freqRange(7,1) & F < freqRange(7,2));
% % %     el_freq_index{8} = find(F >= freqRange(8,1) & F < freqRange(8,2));
% % %     el_freq_index{9} = find(F >= freqRange(9,1) & F < freqRange(9,2));
% % %     el_freq_index{10} = find(F >= freqRange(10,1) & F < freqRange(10,2));
% % %     el_freq_index{11} = find(F >= freqRange(11,1) & F < freqRange(11,2));
% % %     el_freq_index{12} = find(F >= freqRange(12,1) & F <= freqRange(12,2));
