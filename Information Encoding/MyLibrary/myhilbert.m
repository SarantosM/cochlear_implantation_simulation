function [hilbertx] = myhilbert(x,fs,t);
get(0,'Factory');
set(0,'defaultfigurecolor',[1 1 1]);
%x = x(:,1);
N = length(x);
%N = 21;
%x = randn(N,1);
f = fft(x);
complexf = 1i*f;

% find indices of positive and negative frequencies
posF = 2:floor(N/2)+mod(N,2);
negF = ceil(N/2)+1+~mod(N,2):N;

f(posF) = f(posF) + -1i*complexf(posF);
f(negF) = f(negF) +  1i*complexf(negF);

% f(posF) = f(posF)*2;
% f(negF) = f(negF)*0;

hilbertx = ifft(f);

% compare with Matlab function hilbert
%hilbertm = hilbert(x);

% % % % % figure
% % % % % title('The Hilbert Transform')
% % % % % plot3(t*1e3,real(hilbertx),imag(hilbertx))
% % % % % xlabel('Time (ms)'), ylabel('Real part'), zlabel('Imaginary part')
% % % % % axis tight
% % % % % rotate3d
% plot results
figure
subplot(211)
%plot(abs(hilbertm))
%hold on
plot(t*1e3,x,'b'); hold on;
plot(t*1e3,abs(hilbertx),'r-')
xlim([0 1/fs*N*1e3])
xlabel('time [ms]'); ylabel('Amplitude')
legend({'Signal';'Envelope'})
title('Magnitude of the Hilbert transform - Envelope')

subplot(212)
plot(t*1e3,angle(hilbertx),'b')
xlim([0 1/fs*N*1e3]);
title('Phase of the Hilbert transform - Fine Structure')
xlabel('time [ms]'); ylabel('Amplitude')


