function [P, freq, fmean]= myper(x,Fs)
N = length(x);
%n = nextpow2(N);
%N = 2^n;
if mod(N,2) == 1
     x(length(x)+1) = 0;
else
     x = x;
end
N = length(x);
xdft = fft(x);
xdft = xdft(1:N/2+1);
P = (1/(Fs*N)) * abs(xdft).^2;
P(2:end-1) = 2*P(2:end-1);
freq = (0:Fs/length(x):Fs/2)';
figure
plot(freq,10*log10(P))
a = find(P == max(P));
c = abs(P-mean(P));
d = find(c == min(c));
title(['Power Spectral Density: fmax =', num2str(freq(a)),' [Hz]',', fmean ='...
    ,num2str(freq(d)),' [Hz],',' Fs = ',num2str(Fs),' [Hz]'])
xlabel('frequency [Hz]')
ylabel('[dB/Hz]')
end

