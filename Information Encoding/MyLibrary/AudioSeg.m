function [] = AudioSeg(myPath,audio_filename,trange,new_name)
%Write MonoSegment into .wav
[audio,fs] = audioread(strcat(myPath,"/INPUT/Audio/",audio_filename));
if size(audio,2) > 1
    audio = audio(:,1)+audio(:,2);
end

audio = audio/max(abs(audio));

t1 = trange(1);          %30;%60*3.4;    %30 -31 bill evans
t2 = trange(2);          %31;%60*3.5;
n1 = round(fs*t1);
n2 = round(fs*t2);
soundsc(audio(n1:n2),fs);
x = audio(n1:n2);
tend = length(x)*1/fs;
disp(['Time of Segment is ',num2str(tend),' seconds']);
disp(['fs = ',num2str(fs),' [Hz], dt = ',num2str(1/fs*1e3),' ms, N = ',num2str(length(x))]);

audiowrite(strcat(myPath,"/INPUT/Audio/",new_name),x,fs);%,'BitsPerSample',16);