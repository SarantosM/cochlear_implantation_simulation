function [y] = logcompress(x,p);

%xmin and xmax is mapped to the electrical dynamic range [THR,MCL]
%THR : Threshold level
%MCL : most comfortable level measured in microAmps
xh = myhilbert(x);
xh = angle(xh); %abs(xh)
MCL = 300 *1e-6;
THR = 1e-3 * 1000;

A = (MCL-THR)/(max(xh).^p - min(xh).^p);
B = THR -( A * min(xh).^p);
y = A .* xh.^p + B;

figure; plot(y);
%y = A*log(x) + B
