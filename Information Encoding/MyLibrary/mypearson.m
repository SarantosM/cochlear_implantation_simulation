function [xout,cova,pearson] = mypearson(x)

[N,C] = size(x);
%x = (x - ones(N,1)*mean(x));
for i = 1 : C
    x(:,i) = m0v1(x(:,i));
%     std1 = std(x(:,i))
%     std = std(x(:,i))*std1;
end

cova = (1/(N-1))*x'*x;
pearson = (1/(N-1))*x(:,1)'*x(:,2)
xout = x;
imagesc(cov); colorbar; colormap;

