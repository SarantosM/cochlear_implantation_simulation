function [x] = getaudio(fs,tmax)
%Sample your own voice
%Fs = 44100;
%tmax = 4;
nbits = 16;
nchan = 1;
Recorder = audiorecorder(fs, nbits, nchan);
record(Recorder);
fprintf(1, 'Recording ... \n');
pause(tmax);
stop(Recorder);
%Convert to floating-point vector and play back
xi = getaudiodata(Recorder, 'int16');
x = double(xi);
x = x/max(abs(x));
N = length(x);

plotfft(x,fs,[20 fs/2]);
soundsc(x,fs);