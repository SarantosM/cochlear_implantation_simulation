function [ccr1] = mycor(x,y)
x = x(:);       %Ensure Column Vector
N = length(x);  
for tau = 0 : N - 1     %Iterate for all lags
    ac = 0;             %Initialize autocorrelation
    for i = tau + 1 : N %Iterate for tau+1 to N in order to sum
        ac = ac + x(i) * y(i-tau);  %Perform summation and autocorrelation
    end
    ccr1(tau + 1) = ac; %Assign result to output
end
% %ccr1 = ccr1/max(ccr1);
% % figure
% plot(0:N-1,(ccr1/max(ccr1)))
% %title('One Sided Cross Correlation')
% xlabel('tau')
% ylabel('Amplitude')
% % hold on;
% % ccr2 = conv(x,flipud(y));
% % plot(0:N-1,(ccr2/max(ccr2)))
% % legend(['One sided'],['Conv'])
end
