function [centroids] = kMeansInitCentroids(X,k)
%UNTITLED2 Summary of this function goes here
%   Detailed explanation goes here
n_samples = length(X(:,1));
centroids = zeros(k,length(X(1,:)));
for a = 1:k
    idx_cluster = randi(n_samples);
    centroids(a,:) = X(idx_cluster,:);
end
end

