function [xout] = m0v1(x)
N = size(x,1);
x = (x - ones(N,1)*mean(x));

for i = 1 : size(x,2)
    x(:,i) = x(:,i)/std(x(:,i));
end
xout = x;
end