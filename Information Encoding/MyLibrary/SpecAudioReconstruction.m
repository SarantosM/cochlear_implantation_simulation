function [xr] = SpecAudioReconstruction(P)

%fs./(2.^(1:10))



image = [P; flipud(P)];
[nrows ncolumns] = size(image);   
xr = [];

    % Take the ifft of each column of pixels and piece together the results.
for i = 1 : ncolumns

    spectrogramWindow = image(:, i);
    signalWindow = real(ifft(spectrogramWindow));
    xr = [xr; signalWindow];
end
%soundsc(signal,fs)