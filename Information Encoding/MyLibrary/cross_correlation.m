clear all
clc

%illustrating one- and two-sided correlation
%set up data sequences

x = [1:4]';
y = [6:9]';
N = length(x);
% two-sided correlation
ccr2 = zeros(2 * N - 1, 1);
for tau = -N + 1: N - 1
    cc = 0;
    for idx = 1 : N
        lagidx = idx - tau;
        if((lagidx >= 1) && (lagidx <= N))
            cc = cc + x(idx) * y(lagidx);
        end
    end
    ccr2(tau + N) = cc;
end
disp(ccr2)
%one - sided correlation
ccr1 = zeros(N, 1);
for tau = 0 : N - 1
    cc = 0;
    for idx = tau + 1:N
        lagidx = idx - tau;
        cc = cc + x(idx) * y(lagidx);
    end
    ccr1(tau + 1) = cc;
end
disp(ccr1)

%using convolution to calculate correlation
x = [1:4]';
y = [6:9]';
cc = conv(x, flipud(y));
disp(cc)
        