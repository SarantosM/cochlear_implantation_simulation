unction [x,xn] = musictone(Fs,T,note);

[ndata, text] = xlsread('musicfreq.xlsx')
f = ndata(:,1);
t = 0:1/Fs:T;
N = length(t);
A = 1;
w = 2*pi*f(note);
phase = pi/16;
for i = 1:length(note)
    hold on;
    xn(:,i) = real(A*exp(-j*w(i)*t + phase));
    plot(t,xn(:,i))
    legend(num2str(f(note(i))))
end
x = sum(xn,2);
%soundsc(x,Fs);
hold off;
figure
plot(t,x);
title(['Time series of notes',f(note)])
end
