function [y,fs] = SmoothAudioEnds(x,fs,tspan)
x = x(:);
y = x;
N = length(x);
tend = N/fs;
t = 0:1/fs:tend; t = t(1:end-1);
wnd = zeros(1,round(tspan*fs));

% [A] = find(max(abs(x(1:length(wnd)))));
% A1 = 
% x(length(wnd)+1) 

y(1:length(wnd)) = x(1:length(wnd))*0.01;
y(end -length(wnd):end) = x(end -length(wnd):end)*0.001;


plot(t,x,'b-'); hold on;
plot(t(1:length(wnd)),x(1:length(wnd)),'r'); hold on;
plot(t(end -length(wnd):end),x(end-length(wnd):end),'r');

