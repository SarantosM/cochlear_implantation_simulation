%Electromagnetic Theory - Properties

%Constants
h_p = 6.62607015e-35;        %[J*s]
h_d = h_p/(2*pi);
c = 3e9;                    %[m/s]
N_A = 6.02214129e23           %[1/mol]
% df = 1;                     %[Hz]
% f = 10.^(6:df:20);          %[Hz]
% E = h.*f;                   %[J]
% lambda = c./f;              %[m]
% 

%Dimension analysis
%Fundamental Properties
syms Mass Length Time Charge

Energy = Mass*(Length/(Time^2))^2
Power = Energy/Time
c = Mass/Time
h_Planck = Energy*Time
%N_avogadro = 1/mol

%Electromagnetic Variables
%Joule = Energy;
%Newton = 
Coulomb = Charge;   %Essentially a Flux,
Ampere = Coulomb/Time;
Volt = Mass * Length^2 * Time^(-2) * Charge^(-1)
Farad = Coulomb/Volt
Meter = Length;
%Electromagnetic Radiation - Coulomb's and Amper's law
Charge_electron = 1.6021766208e-19
%F = Charge^2
Tesla = Mass * Time^(-2)*Ampere^(-1)
%Fluxes and Diffusion
Resistance = Volt/Ampere;
Diffusion = Length^(2)/Time
%charge = Time

epsilon = Farad/Meter; 
Force = Charge^2/(epsilon*Length^2)
Capacitance = Coulomb/Volt;

siemens = 1/Resistance;
CurrentDensity = Ampere/Length^2;

% rho = siemens;
% phi = CurrentDensity;
% sigma = Volt;
% %divergence = 
% divergence = 1/Length^1;
% gradient3 = 1/Length^1;
% leftside = divergence*sigma*(gradient3*phi)
% rightside = rho/Time

%Poisson's equation
rho = Charge/Length^3;
phi = Volt;
%sigma = rho/Length;%divergence =
sigma = CurrentDensity/(Volt/Length)
divergence = 1/Length^1;
gradient = 1/Length^1;
leftside = divergence*sigma*(gradient*phi)
rightside = rho/Time
% I_current = Charge/Time     %Current
% V_voltage =                 %Voltage
% R_resistance =                 %
% C_capacitance = Charge/V_Voltage   %[Farad]
% Farad = 
