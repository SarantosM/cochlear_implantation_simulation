function [centroids] = computeMeans(X, idx, k)

centroids = zeros(k,length(X(1,:)));
for a = 1:k
    centroids(a,:) = mean(X((idx==a),:));
end

end

