function [A,Elec] = findChannel(f,BW)  %(x,fs,n,BW)
%f = fchord;
%BW = BWElec;

for i = 1:length(f)
    for j = 1:length(BW)
        if f(i) >= BW(j,1) && f(i) <= BW(j,2)
            Elec(i) = j;

        end
    end
end

A = ones(length(f),1);
[a,b] = histc(Elec,unique(Elec));
y = a(b);
%if length(unique(Elec)) <= length(Elec)
for i = 1:length(f)
    A(i) = 1./y(i);
end

    
