%Post-Processing



[tmp,nervesc,nodes,body,Idx,Idx_c] = topology(Potential,pwd);


col = {'b','r'};
a = 1:6; %[1 8];
for i = 1:length(a)
    
    plot3(nodes(body{a(i),1},1),nodes(body{a(i),1},2),nodes(body{a(i),1},3),'o'); %strcat(col(i),'o')
    hold on;
end
legend(['Scala Media'],['Electrode Array']);

handle = axes('Parent', figure());
hold(handle, 'off');

nod = [nodes(body{1,1},:);nervesc]; 
for i = 1:TS
    %hold on;
    %title(['t = ' num2str(t(k)),' sigma=',num2str(sigma),' beta=',num2str(beta)...
    %    ,' r =',num2str(rho),' IC: ',num2str(IC)])
    %axis = [min(x), max(x), min(y), max(y),min(z), max(z)];
    %plot3(nod(i,1),nod(i,2),nod(i,3),'bo')
    plot(Potential(Idx,i),'b')
    ylim([min(min(Potential)) max(max(Potential))])
    title(['t = ',num2str(time_fem(i)*1e3), ' ms'])
    %view(3)
    xlabel('x'); ylabel('y'); zlabel('z');
    %colorbar(Potential(Idx(1),i));
    %plot3()
    drawnow
end
    %view(k+1,45)
     %view(0,30);
    %view(0+k,45+k*0.5 )
    pause(0.1)    
hold off;

figure

for i = 1:length(nod)
    hold on;
    plot3(nod(:,1),nod(:,2),nod(:,3),'Color',[0 0 0],'Marker','o')
end


tnodes = 0;
for i = 1:18
    tnodes = tnodes + length(body{i,1});
end
    
tnodes = sum(length(body{:,1}),1)

plot3(nodes(body{18,1},1),nodes(body{18,1},2),nodes(body{18,1},3),'bo')



Input_Output(signalx(1:TS),fs_fem,Potential,pwd);

a = [5 7:18];
for i = 1:length(a)
    if i == 1
        plot3(nodes(body{a(i),1},1),nodes(body{a(i),1},2),nodes(body{a(i),1},3),'ro') %,'Color',[0 0 0]+ 0.5); %strcat(col(i),'o')
        hold on;
    else
        plot3(nodes(body{a(i),1},1),nodes(body{a(i),1},2),nodes(body{a(i),1},3),'ko');
        hold on;
    end
end


a = [1 2 3];
for i = 1:length(a)
    
    plot3(nodes(body{a(i),1},1),nodes(body{a(i),1},2),nodes(body{a(i),1},3),'o'); %strcat(col(i),'o')
    hold on;
end
legend(['Scala Media'],['Electrode Array']);


%%%PLot compartment
plot3(nervesc(:,1),nervesc(:,2),nervesc(:,3),'bo'); hold on;
for i = 1:47
    plot3(Idx_c{i,334}(1),Idx_c{i,334}(2),Idx_c{i,334}(3),'ro');
    plot3(Idx_c{i,1}(1),Idx_c{i,1}(2),Idx_c{i,1}(3),'ro');
end

plot3(nodes(Idx,1),nodes(Idx,2),nodes(Idx,3),'Marker','o','Color',[0 0 0]);


handle = axes('Parent', figure());
hold(handle, 'off');

nod = [nodes(body{1,1},:);nervesc]; 
for i = 1:TS
    %hold on;
    %title(['t = ' num2str(t(k)),' sigma=',num2str(sigma),' beta=',num2str(beta)...
    %    ,' r =',num2str(rho),' IC: ',num2str(IC)])
    %axis = [min(x), max(x), min(y), max(y),min(z), max(z)];
    %plot3(nod(i,1),nod(i,2),nod(i,3),'bo')
    plot(Potential(Idx,i),'b')
    ylim([min(min(Potential)) max(max(Potential))])
    title(['t = ',num2str(time_fem(i)*1e3), ' ms'])
    %view(3)
    xlabel('x'); ylabel('y'); zlabel('z');
    %colorbar(Potential(Idx(1),i));
    %plot3()
    drawnow
end