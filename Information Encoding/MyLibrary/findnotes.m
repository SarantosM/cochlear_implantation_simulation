function [Notes,dist] = findnotes(f,nNeighbours)
[ndata, text] = xlsread('musicfreq.xlsx');
freq = ndata(:,1);
%nNeighbours = 2;
[Idx dist] = knnsearch(freq,f','k',nNeighbours);

Notes = text(Idx);




% Notes = {'C4','E4','G4','B4'};
% 
% % dt = 0.0005;
% % TS = 300;
% % t = 0:dt:dt*TS;
% % t = t(1:end-1);
% for i = 1:length(Notes)
%     dummy = strfind(text_notes, Notes{i});
%     Index{i} = find(~cellfun(@isempty,dummy));
%     fchord(i) = freq_notes(Index{i});
% end