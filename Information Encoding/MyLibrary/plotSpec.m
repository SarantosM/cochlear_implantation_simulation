function [] = plotSpec(x,fs,f_over,cmap,n1,n2,t_wnd)
x = x(n1*fs:n2*fs);
%soundsc(x,fs);

%nfft = ceil(fs/70);
nfft = round(t_wnd*fs);
nfreq = ceil((1+nfft)/2);
noverlap = round(nfft*f_over);
nframes = fix((length(x)-nfft)/noverlap);

[STFT, f, t] = stft(x, hamming(nfft), noverlap, nfft, fs);

PdB = 10*log10(abs(STFT)./max(abs(STFT)));
%PdB = 10*log10(abs(STFT));
figure
imagesc(t*1e3,fliplr(f),(PdB))        %,clipvals);
xlabel('Time (ms)');
ylabel('Freq (Hz)');
colorbar;
hc=colorbar;
title(hc,'dB');
colormap(cmap);
title(['Spectrogram at nfft: nframes = ',num2str(nframes),', nfreq = ',num2str(nfreq),', wnd = ',num2str(nfft*fs)...
    ,'[ms], dt = ',num2str(mean(diff(t))*1e3),' [ms]']);
% hax = gca;
% hax.YTickLabel = flipud(hax.YTickLabel);
% 
% [X, F, T, P] = spectrogram(x, hamming(nfft,'periodic'), noverlap, nfft, fs,'onesided');
% tend = length(x)/fs;
% d_t = tend/nframes
% %%%%%--------Spectrogram--------------------------
% 
% PdB = 10*log10(abs(P)./max(abs(P)));
% figure
% imagesc(T*1e3,F,PdB)        %,clipvals);
% xlabel('Time (ms)');
% ylabel('Freq (Hz)');
% colorbar;
% hc=colorbar;
% title(hc,'dB');
% colormap(cmap);
% title(['Spectrogram at nfft: nframes = ',num2str(nframes),', nfreq = ',num2str(nfreq),', wnd = ',num2str(nfft*fs*1e3)...
%     ,'[ms], dt = ',num2str(d_t*1e3),' [ms]']);
