function [CHORD] = chord_recognition(x,fs)

% clear all
% clc
% %x = getaudio(17200,1);
% fs = 17200;
% NOTES = {'A3' 'C#4/Db4' 'E4' 'G#4/Ab4'}
% tend = 0.4;
% time = 0:1/fs:tend; time = time(1:end-1);
% [swave,f] = chordgen(ones(length(NOTES),1),NOTES,time,fs,1);

text_notes = {'C0';' C#0/Db0 ';'D0';' D#0/Eb0 ';'E0';'F0';' F#0/Gb0 ';'G0'...
    ;' G#0/Ab0 ';'A0';' A#0/Bb0 ';'B0';'C1';' C#1/Db1 ';'D1';' D#1/Eb1 ';...
    'E1';'F1';' F#1/Gb1 ';'G1';' G#1/Ab1 ';'A1';' A#1/Bb1 ';'B1';'C2';...
    ' C#2/Db2 ';'D2';' D#2/Eb2 ';'E2';'F2';' F#2/Gb2 ';'G2';' G#2/Ab2 ';...
    'A2';' A#2/Bb2 ';'B2';'C3';' C#3/Db3 ';'D3';' D#3/Eb3 ';'E3';'F3';...
    ' F#3/Gb3 ';'G3';' G#3/Ab3 ';'A3';' A#3/Bb3 ';'B3';'C4';' C#4/Db4 ';...
    'D4';' D#4/Eb4 ';'E4';'F4';' F#4/Gb4 ';'G4';' G#4/Ab4 ';'A4';...
    'A#4/Bb4 ';'B4';'C5';' C#5/Db5 ';'D5';' D#5/Eb5 ';'E5';'F5';...
    ' F#5/Gb5 ';'G5';' G#5/Ab5 ';'A5';' A#5/Bb5 ';'B5';'C6';' C#6/Db6 ';...
    'D6';' D#6/Eb6 ';'E6';'F6';' F#6/Gb6 ';'G6';' G#6/Ab6 ';'A6';' A#6/Bb6 ';...
    'B6';'C7';' C#7/Db7 ';'D7';' D#7/Eb7 ';'E7';'F7';' F#7/Gb7 ';'G7';...
    ' G#7/Ab7 ';'A7';' A#7/Bb7 ';'B7';'C8';' C#8/Db8 ';'D8';' D#8/Eb8 ';...
    'E8';'F8';' F#8/Gb8 ';'G8';' G#8/Ab8 ';'A8';' A#8/Bb8 ';'B8'};
for i = 1:length(text_notes)
    freq_notes(i,1) = 440*2^((-58+i)/12);
end

note_l = 'C2';
dummy = strfind(text_notes, note_l);
idx1 = find(~cellfun(@isempty,dummy));

note_h = 'B5';
dummy = strfind(text_notes, note_h);
idx2 = find(~cellfun(@isempty,dummy));

f_notes = freq_notes(idx1:idx2);
t_notes = text_notes(idx1:idx2);

if ceil(2.2*freq_notes(idx2)) < fs
    disp(['Resampling at fs = ',num2str(ceil(2.2*freq_notes(idx2))),' Hz'])
    y = resample(x,ceil(2.2*freq_notes(idx2)),fs);
    fs = ceil(2.2*freq_notes(idx2));
else
    y = x;
end
soundsc(y,fs)
freq = linspace(0,fs/2,floor(length(y)/2)+1);

[Idx dist] = knnsearch(freq',f_notes,'k',1);

X = fft(y);
X_mag = abs(X(1:floor(length(y)/2)+1));

%obw(y,fs,[f_notes(1) f_notes(end)]);
n = 4;

[powsort idx3] = sort(X_mag(Idx),'descend');
%powsort idx3] = sort(X_mag,'descend');
for i = 1:n
    chord{i,1} = t_notes{idx3(i)};
end

%Order from lowest to highest Frequency
for i = 1:length(chord)
    dummy = strfind(t_notes, chord{i});
    idx4(i) = find(~cellfun(@isempty,dummy));
end

idx5 = sort(idx4,'ascend');

for i = 1:length(chord)
    chord2{i,1} = t_notes{idx5(i)};
end

for i = 1:length(chord)-1
    idx6(i) = idx5(i+1)-idx5(1);
end
interval = [];
seven = [];
for i = 1:length(idx6)
    if idx6(i) == 3
        interval = 'min'
    else if idx6(i) == 4
        interval = 'maj'
        end
    end
    if idx6(i) == 10
        seven = 'b7'
    else if idx6(i) == 11
        seven = '7'
        end
    end
end

if isempty(seven) == 0
    chord_name = strcat(chord2{1,1}(1:2),interval,seven)
else
    chord_name = strcat(chord2{1,1}(1:2),interval)
end

CHORD.name = chord_name;
CHORD.f_notes = f_notes;
CHORD.t_notes = t_notes;
CHORD.chord1 = chord;
CHORD.chord2 = chord2;
CHORD.idx5 = idx5;
CHORD.idx6 = idx6;
CHORD.y = y;
CHORD.fs = fs;
% % % t_Intervals = {min maj}
% % % 
% % % 
% % % 
% % % [swave,f] = chordgen(ones(length(chord),1),chord,(0:length(y)-1)/fs,fs,1);
% % % 
% % % 
% % % chord = sort((idx3(1:n)));

% % zp = zeros(length(x),1);
% % for i = 1:length(zp)
% %     freq_z = linspace(0,fs/2,floor(length(y)+i/2)+1);
% %     [Idx2 dist2] = knnsearch(freq',f_notes,'k',1);
% %     if mean(dist2) <= 0.1
% %         a = i;
% %     end
% % end