function [tmp,nervesc,nodes,body,Idx,Idx_c] = topology(V,myPath)


%mypath = "/home/saradis/Documents/Thesis/"
nervesc = csvread(strcat(myPath,'\INPUT\NervesCoord.csv'));
%cno = csvread('cochleaNodes_out.csv');

%nodes = dlmread(strcat(mypath,'\mesh.nodes'),'',0,0);
nodes = dlmread(strcat(myPath,'\INPUT\Mesh\mesh.nodes'),'',0,0);
%save nodes.mat
nodes = nodes(:,3:end);
BC = dlmread(strcat(myPath,'\INPUT\Mesh\mesh.boundary'),'',0,0);  %Windows
%NP = load(['nervesPotential1.csv']);
%BC = dlmread(strcat(mypath,'mesh.boundary'),'',0,0);   %Linux
%%%%%%%%%%----Import File-----%%%%%%%%%%%%
% nodes = dlmread('/home/saradis/Documents/PYTHON/HearEU/cmear/mesh_generation/model/nodes.txt',' ',0,0); 
% BC = dlmread('/home/saradis/Documents/PYTHON/HearEU/cmear/mesh_generation/model/BC.txt',' ',0,0);



%-------------Get Indexes of BC ---------------------------------
A = [BC(:,2) BC(:,6:8)];
for i = 1 : 18
    for j = 1 : length(A)
        if A(j,1) == i
            b{i} = j;
        end
    end
end

for i = 1:18
    if i == 1
        body{i,:} = unique(A(1 : b{1} ,2 : 4));
    else
        body{i,:} = unique(A(b{i-1}  +1 : b{i},2:4));
    end
end

body{1,2} = 'scala';
body{2,2} = 'brain';
body{3,2} = 'bone';
body{4,2} = 'scalp';
body{5,2} = 'array';
body{6,2} = 'ext_elec';

for i = 1:12
    body{6+i,2} = strcat('E',num2str(13-i));
end

%     Elec_coord{i} = nodes(body{i},:);
% end


% for i = 1:18
%     scala_coord = nodes(body{1},:); 
%     brain_coord = nodes(body{2},:);
%     bone_coord = nodes(body{3},:);
%     scalp_coord = nodes(body{4},:);
%     array_coord = nodes(body{5},:);
%     ext_elec_coord = nodes(body{6},:);
%     Elec_coord{i} = nodes(body{i},:);
% end


nNeighbours = 1;
[Idx dist] = knnsearch(nodes,nervesc,'k',nNeighbours);


TS = size(V,2);
tmp = zeros(47,334,TS);

a = 0;

for j = 1:334
    for i = 1:47
        for t = 1:TS  %TS %size(Potential,2)            
            if j == 1
                Idx_c{i,j} = nervesc(i,:);  %nodes(Idx(i),:);
                tmp(i,j,t) = V(i,t);
            else if j == 334
                a = i + length(nervesc) - 47;
                Idx_c{i,j} = nervesc(a,:);   %nodes(Idx(a),:);
                tmp(i,j,t) = V(a,t);
            else
                a = i + (j-1)*47;
                Idx_c{i,j} = nervesc(a,:);   %nodes(Idx(a),:);
                tmp(i,j,t) = V(a,t);
        %Idx_c{i,j} = nervesc(i,:);
                end
            end
        end
    end
end



% TS = size(V,2);
% tmp = zeros(334,47,TS);
% 
% 
% for j = 1:47
%     for i = 1:334
%         for t = 1:TS  %TS %size(Potential,2)
%         %if i <= 334
%             tmp(i,j,t) = V(Idx(i),t);
%             Idx_c{i,j} = nodes(Idx(i),:);
%         %Idx_c{i,j} = nervesc(i,:);
%         end
%     end
% end


% sigma = [1.43 0.2 0.0153846153846 0.33 1e-7 1e3 1e3 1e3 1e3 1e3...
%     1e3 1e3 1e3 1e3 1e3 1e3 1e3 1e3];

