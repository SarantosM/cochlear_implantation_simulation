function [fc_notes,BW_notes,freq,text] = findElec_notes(fc,BW)  %(x,fs,n,BW)


[ndata, text] = xlsread('musicfreq.xlsx');
freq = ndata(:,1);
nNeighbours = 2;
[Idx dist] = knnsearch(freq,fc','k',nNeighbours);

%fc_notes = cell(length(fc));
for i = 1:length(fc)
    %fc_notes = ;
    %Index = strfind(text,
    fc_notes{i} = text{Idx(i,1),1};
end
    
for i = 1:length(BW)
    if i == length(BW)
        [index] = find(freq >= BW(i,1) & freq <= BW(i,2));    
        BW_notes{i,1} = freq(index,1);
        BW_notes{i,2} = text(index,1);
    else
        [index] = find(freq >= BW(i,1) & freq < BW(i,2));    
        BW_notes{i,1} = freq(index,1);
        BW_notes{i,2} = text(index,1);
    end     
end
    

    
    
    
%format rat
%just = [1 16/15 9/8 6/5 5/4 4/3 7/5 3/2 8/5 5/3 7/4 15/8 2]'
% format short
% sigma = 2^(1/12);
% k = (0:12)';
% equal = sigma.^k;
% num = [1 16 9 6 5 4 7 3 8 5 7 15 2]';
% den = [1 15 8 5 4 3 5 2 5 3 4  8 1]';
% just = num./den;
% delta = (equal - just)./equal;
% T = [k equal num den just delta];
% fprintf('   k       equal           just              delta\n')
% fprintf('%4d %12.6f %7d/%d %11.6f %10.4f\n',T')

%for 
% plot(just)
% hold on; plot(equal,'r')
% legend(['Just Intonation'],['Equal Temperament'])
% f = 987.77*equal;
% w = 2*pi*f;
% A = 1;
% % x(length(t),length(f)) = real(A*exp(-j*t'.*w'));
