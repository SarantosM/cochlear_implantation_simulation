function [wlen_min,t_wnd_min,nfreq,nbins,binloss,nfft_min,nframes,d_t] = SpecFEM_nfft_min(x,fs,BWElec,f_ovr) %,TSmax,dtmin)
prc = 0.3;
N = length(x);
tend = N/fs;
%exponent = ceil(log(wlen_min)/log(2));
wlen_min = ceil(fs/BWElec(1,1));     %Lowest frequency of Electrode Array
t_wnd_min = wlen_min/fs;            %[s]                             
nfft_min = wlen_min;
nfreq = ceil((1+nfft_min)/2); % calculate the number of unique fft points

%nfft = 2*nfreq - 2;
F = linspace(0,fs/2,nfreq);

binBW = mean(diff(F));

for i = 1:12
    if i == 12
        el_freq_index{i} = find(F >= BWElec(i,1) & F <= BWElec(i,2));    
    
        binloss{i} = 1 - length(el_freq_index{i})/length(BWElec(i,1):prc:BWElec(i,2));
    else
        el_freq_index{i} = find(F >= BWElec(i,1) & F < BWElec(i,2));    
    
        binloss{i} = 1 - length(el_freq_index{i})/length(BWElec(i,1):prc:BWElec(i,2));
    end  
    nbins{i} = length(el_freq_index{i});
end

% % figure
% % binloss = cell2mat(binloss);
% % plot(binloss)
% % title(['Binloss for each Electrode BW: # of Perceptible frequencies between 1 Hz = ',num2str(ceil(1/prc))]);
% % xlabel('# of Electrode'); ylabel('Binloss')
% nbins = cell2mat(nbins)
% figure
% plot(nbins)

noverlap = nfft_min*f_ovr; 
nframes= 1+fix((N-wlen_min)/ceil((1-f_ovr)*wlen_min)); % calculate the number of signal frames
d_t = tend/nframes;

% wnd= hamming(nfft_min,'periodic');
% 
% [Y, freq, T, P] = spectrogram (x, wnd, noverlap, nfft, fs,'onesided');
% if size(P,1) < 12
%     print('Rows are less than 12')
% end




% subplot(211)
% plot(time .* 1e3,stimuli{a}); xlabel('time [ms]');ylabel('V')
% subplot(212)
% plot(time .* 1e3, x);xlabel('time [ms]');ylabel('V')

% 
% STFT = zeros(NUP, TS);       % preallocate the stft matrix
% % STFT (via time-localized FFT)
% hop = noverlap;
% wlen = length(wnd);
% for i = 0:TS-1
%     % windowing
%     xw = x(1+i*hop : wlen+i*hop).*wnd;
%     
%     % FFT
%     X = fft(xw, nfft);
%     
%     % update of the stft matrix
%     STFT(:, 1+i) = X(1:NUP);
% end
% % calculation of the time and frequency vectors
% t = (wlen/2:hop:wlen/2+(L-1)*hop)/fs;
% f = (0:NUP-1)*fs/nfft;
