function [d_t,tend,Fmax,Fmin,logical] = find_d_t(TS_max,fs,time_range,BW,c)

tmin = time_range(1);
tmax = time_range(2);
dt = linspace(1/fs,1e-2,100000);
Fs = 1./dt; 

%TS = N;
%N = 500;

tend = TS_max .* dt;

freq_res = Fs/TS_max;

%Bounds
Fmax = Fs/2;
Fmin = freq_res;

%a = Elmax;

%tmax = 0.3;
%BW = BWElec(a,:);
[index] = find(Fmin < BW(1,1) & Fmax > BW(1,2) & tend < tmax);

dt = dt(index);

tendS = dt*TS_max;
clear Fmin Fmax tend
for i = 1:3
    if i == 1
        [Idt] = find(dt > 10e-5 & dt < 0.5*1e-3 & tendS >= tmin);
        disp('-----------------------------------------------------------------------')
        d_t{i} = dt(Idt);
        if isempty(d_t{i}) == 0
            disp('Phase Locking')
            tend{i} = TS_max * d_t{i};
            Fmax{i} = 1 ./(2*d_t{i});
            Fmin{i} = 1 ./ tend{i};
            X = ['dt ',num2str(d_t{i}(1).*1e3),' - ',num2str(d_t{i}(end)*1e3),...
                '[ms] Physical Time = ',num2str(tend{i}(1).*1e3),' - ',...
                num2str(tend{i}(end)*1e3),' [ms], ','BWmean = ',num2str(round(mean(Fmin{i}(1)))),'[Hz] ',num2str(round(mean(Fmax{i}(1))))];
            %'meandt = ',num2str(mean(d_t{c})*1e3),'ms ',
            disp(X);
            disp('-----------------------------------------------------------------------')
        else 
            disp('Not available time steps for this Phase Locking and parameters')
            tend{i} = [];
            Fmax{i} = [];
            Fmin{i} = [];
        end

        
    elseif i == 2
        [Idt] = find(dt >= 0.3*1e-3 & dt <= 0.5*1e-3 & tendS >= tmin);
                
        d_t{i} = dt(Idt);

        if isempty(d_t{i}) == 0
            disp('Neural Spike Travel Time')
            tend{i} = TS_max * d_t{i};
            Fmax{i} = 1 ./(2*d_t{i});
            Fmin{i} = 1 ./ tend{i};
            X = ['dt ',num2str(d_t{i}(1).*1e3),' - ',num2str(d_t{i}(end)*1e3),...
                '[ms] Physical Time = ',num2str(tend{i}(1).*1e3),' - ',...
                num2str(tend{i}(end)*1e3),' [ms], ','BWmean = ',num2str(round(mean(Fmin{i}(1)))),'[Hz] ',num2str(round(mean(Fmax{i}(1))))];
            disp(X);
            disp('-----------------------------------------------------------------------')
        else 
            disp('Not available time steps for this Neural Spike Travel Time range and parameters')
            tend{i} = [];
            Fmax{i} = [];
            Fmin{i} = [];
        end

    elseif i == 3

        [Idt] = find(dt >=  0.5*1e-3 & dt <= 1e-3 & tendS >= tmin);
        %disp('Longer than NSTT - less than ANF refractory period')
        d_t{i} = dt(Idt);
        if isempty(d_t{i}) == 0
            disp('Longer than NSTT - less than ANF refractory period')
            tend{i} = TS_max * d_t{i};
            Fmax{i} = 1./(2*d_t{i});
            Fmin{i} = 1 ./ tend{i};
            X = ['dt ',num2str(d_t{i}(1).*1e3),' - ',num2str(d_t{i}(end)*1e3),...
                '[ms] Physical Time = ',num2str(tend{i}(1).*1e3),' - ',...
                num2str(tend{i}(end)*1e3),' [ms], ','BWmean = ',num2str(round(mean(Fmin{i}(1)))),'[Hz] ',num2str(round(mean(Fmax{i}(1))))];
            disp(X);
            disp('-----------------------------------------------------------------------')
        else 
            disp('Not available time steps for larger than ANF refractory period and these selected parameters')
            tend{i} = [];
            Fmax{i} = [];
            Fmin{i} = [];
        end
    end
    
end

% count = 0;
% while count < 3
% switch c
%     case 'all'
if c == 0   %For function in information encoding to find at least one non empty matrix
    if (isempty(d_t{1}) == 1) && (isempty(d_t{2})) == 1 && (isempty(d_t{3}) ==1)
        logical = 0;
    else
        logical = 1;
    end
elseif c == 1 %For function in information encoding to find at least one non empty matrix in the Phase Locking
    if (isempty(d_t{1}) == 0)
        logical = 1;
    else
        logical = 0;
    end
elseif c == 2 %For function in information encoding to find at least one non empty matrix in the Neural Spike Time
    if (isempty(d_t{2}) == 0)
        logical = 1;
    else
        logical = 0;
    end
else
    logical = 0;
end


%     case 'PhaseLocking'
%         if (isempty(d_t{1}) == 0)
%             logical = 1;
%         else
%             logical = 0;
%         end
% end
% end
%freq_res = Fs/N;

%Bounds
%     Fmax{c} = Fs/2;
%     Fmin{c} = 1 ./ tend;
% end
% 
% 
% switch c
%     case 1
%         [Idt] = find(dt > 10e-5 & dt < 0.3*1e-3 & tendS >= tmin);
%         disp('Phase Locking')
%     case 2
%         [Idt] = find(dt >= 0.3*1e-3 & dt <= 0.5*1e-3 & tendS >= tmin);
%         disp('Neural Spike Travel Time')
%     case 3
%         [Idt] = find(dt >=  0.5*1e-3 & d_ <= 1e-3 & tendS >= tmin);
%         disp('Longer than NSTT - less than ANF refractory period')
%     otherwise
%         disp('Timestep is greater than ANF Refractory period')
% end

% 
% switch c
%     case 1
%         [Idt] = find(dt > 10e-5 & dt < 0.3*1e-3 & tendS >= tmin);
%         disp('Phase Locking')
%     case 2
%         [Idt] = find(dt >= 0.3*1e-3 & dt <= 0.5*1e-3 & tendS >= tmin);
%         disp('Neural Spike Travel Time')
%     case 3
%         [Idt] = find(dt >=  0.5*1e-3 & d_ <= 1e-3 & tendS >= tmin);
%         disp('Longer than NSTT - less than ANF refractory period')
%     otherwise
%         disp('Timestep is greater than ANF Refractory period')
% end



% for i = 1: length(d_t)
% if d_t(i) < 1e-5
%     disp('Timestep is smaller than depolarization of IHC')
% elseif d_t(i) > 10e-5 & d_t(i) < 0.3*1e-3
%     disp('Phase Locking')
% elseif d_t(i) >= 0.3*1e-3 & d_t(i) <= 0.5*1e-3
%     disp('Neural Spike Travel Time')
% elseif d_t(i) > 1e-3
%     disp('Timestep is greater than ANF Refractory period')
% end
% end
% 
% %tendS > 0.3;
% sw
% [Idt] = find(d_t >= 0.3*1e-3 & d_t <= 0.5*1e-3 & tendS >= tmin);
