function [TS,d_t,tend,Fmax,Fmin] = findBestd_t(fs,TS_max,time_range,BW,c)


for TS = 1:TS_max    
    [d_t,tend,Fmax,Fmin,logical] = find_d_t(TS,fs,time_range,BW,c);
    if logical == 1
       
        disp(['Minimum TS is ',num2str(TS)])
        break
    end
end