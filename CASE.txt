Header
  CHECK KEYWORDS Warn
  Mesh DB "." "."
  Include Path ""
  Results Directory ""
End

Simulation  
  Max Output Level = 5     %5 up to 43 for full feedback on the terminal-log
  Coordinate System = Cartesian
  Coordinate Mapping(3) = 1 2 3
  Simulation Type = Transient
  Steady State Max Iterations = 100
  Output Intervals = 1                                          !Outputs after # timestep interval
  Timestepping Method = BDF
  BDF Order = 1
  Timestep intervals = Integer Procedure "MyTimestep" "MyTS"  !Total time = # * Timestep Sizes               
  Timestep Sizes = Real Procedure "MyTimestep" "Mydt"                     !# in [seconds]
  Coordinate Scaling = 0.001                                              !Convert in [mm] scale
  Solver Input File = case.sif
  Post File = case.vtu
End

Constants
  Gravity(4) = 0 -1 0 9.82
  Stefan Boltzmann = 5.67e-08
  Permittivity of Vacuum = 8.8542e-12
  Boltzmann Constant = 1.3807e-23
  Unit Charge = 1.602e-19
End

!!!-------Scala--------!!!
Body 1
  Target Bodies(1) = 1
  Name = "Body 1"
  Equation = 1
  Material = 1
End
!!!-----Brain---------!!!
Body 2
  Target Bodies(1) = 2
  Name = "Body 2"
  Equation = 1
  Material = 6
End
!!!---Bone-----------!!!
Body 3
  Target Bodies(1) = 3
  Name = "Body 3"
  Equation = 1
  Material = 2
End
!!!----Scalp---------!!!
Body 4
  Target Bodies(1) = 4
  Name = "Body 4"
  Equation = 1
  Material = 5
End
!!!---Electrode Array--!!!
Body 5
  Target Bodies(1) = 5
  Name = "Body 5"
  Equation = 1
  Material = 3
End
!!!--External Electrode for Monopolar(used as ground)---!!!
Body 6
  Target Bodies(1) = 6
  Name = "Body 6"
  Equation = 1
  Material = 4
End

!!!Electrodes
!!!--Elec [6320 8500] Hz---!!!--Base
Body 7
  Target Bodies(1) = 7
  Name = "Body 7"
  Equation = 1
  Material = 4
End
!!!--Elec [4693 6320] Hz---!!!
Body 8
  Target Bodies(1) = 8
  Name = "Body 8"
  Equation = 1
  Material = 4
End
!!!--Elec [3475 4693] Hz---!!!
Body 9
  Target Bodies(1) = 9
  Name = "Body 9"
  Equation = 1
  Material = 4
End
!!!--Elec [2564 3475] Hz---!!!
Body 10
  Target Bodies(1) = 10
  Name = "Body 10"
  Equation = 1
  Material = 4
End
!!!--Elec [1881 2564] Hz---!!!
Body 11
  Target Bodies(1) = 11
  Name = "Body 11"
  Equation = 1
  Material = 4
End
!!!---Elec [1368 1881] Hz---!!!
Body 12
  Target Bodies(1) = 12
  Name = "Body 12"
  Equation = 1
  Material = 4
End
!!!---Elec [982 1368] Hz---!!!
Body 13
  Target Bodies(1) = 13
  Name = "Body 13"
  Equation = 1
  Material = 4
End
!!!---Elec [690 928] Hz---!!!
Body 14
  Target Bodies(1) = 14
  Name = "Body 14"
  Equation = 1
  Material = 4
End
!!!---Elec [469 690] Hz---!!!
Body 15
  Target Bodies(1) = 15
  Name = "Body 15"
  Equation = 1
  Material = 4
End
!!!---Elec [300 469] Hz---!!!
Body 16
  Target Bodies(1) = 16
  Name = "Body 16"
  Equation = 1
  Material = 4
End
!!!---Elec [170 300] Hz---!!!
Body 17
  Target Bodies(1) = 17
  Name = "Body 17"
  Equation = 1
  Material = 4
End
!!!---Elec [70 170] Hz---!!!--Apex
Body 18
  Target Bodies(1) = 18
  Name = "Body 18"
  Equation = 1
  Material = 4
End

Solver 1
  Equation = Static Current Conduction
  Calculate Volume Current = True
  Procedure = "StatCurrentSolve" "StatCurrentSolver"
  Variable = Potential
  Exec Solver = Always
  Stabilize = True
  Bubbles = False
  Lumped Mass Matrix = False
  Optimize Bandwidth = True
  Steady State Convergence Tolerance = 1.0e-5
  Nonlinear System Convergence Tolerance = 1.0e-7
  Nonlinear System Max Iterations = 40              !20
  Nonlinear System Newton After Iterations = 3
  Nonlinear System Newton After Tolerance = 1.0e-3
  Nonlinear System Relaxation Factor = 1
  Linear System Solver = Iterative
  Linear System Iterative Method = BiCGStab
  Linear System Max Iterations = 900                !500
  Linear System Convergence Tolerance = 1.0e-7 ! 1.0e-10
  BiCGstabl polynomial degree = 2
  Linear System Preconditioning = ILU0
  Linear System ILUT Tolerance = 1.0e-3
  Linear System Abort Not Converged = False
  Linear System Residual Output = 10
  Linear System Precondition Recompute = 1
End

Solver 2
  Equation = Result Output
  Save Geometry Ids = True
  Output Format = Vtu
  Procedure = "ResultOutputSolve" "ResultOutputSolver"
  Output File Name = case
  Exec Solver = After Simulation
End

Equation 1
  Name = "Elet"
  Active Solvers(1) = 1
End

Equation 2
  Name = "Paraview"
  Active Solvers(1) = 2
End




Material 1
  Name = "Scala"
  Electric Conductivity = 1.43
End

Material 2
  Name = "Bone"
   Electric Conductivity = 0.0153846153846
End

Material 3
  Name = "Array"
  Electric Conductivity = 1e-7
End

Material 4
  Name = "Platinum elecs"
  Electric Conductivity = 1e3
End

Material 5
  Name = "Scalp"
  Electric Conductivity = 0.33
End


Material 6
  Name = "Brain"
  Electric Conductivity = 0.2
End

!!!-------Monopolar - external electrode set to ground-----!!!
Boundary Condition 1
  Target Boundaries(1) = 6 
  Name = "Ground"
  Potential = 0.0
End

!!!-----External Boundary is an Insulator------------------!!!
Boundary Condition 2
  Target Boundaries(1) = 4 
  Name = "Insulator"
  Current Density BC = True
  Current Density = 0.0 
 End

!!!-------------------------Neural Encoding Scheme for Electrodes-------------------------------------!!!
